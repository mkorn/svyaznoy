# Тестовое задание  
1. Разработать Web-приложение для отправки СМС абоненту.  

**Скриншоты приложения**  
  
Общий вид интерфейса:  
  
[![https://gyazo.com/6db87ef837c4c19beda2e2b4b362967e](https://i.gyazo.com/6db87ef837c4c19beda2e2b4b362967e.png)](https://gyazo.com/6db87ef837c4c19beda2e2b4b362967e)  

Валидация формы:  
  
[![https://gyazo.com/36e18dae178728c420cd334cf11d3dc7](https://i.gyazo.com/36e18dae178728c420cd334cf11d3dc7.png)](https://gyazo.com/36e18dae178728c420cd334cf11d3dc7)  

История отправки SMS с пагинацией:  
  
[![https://gyazo.com/88623573a0531e5133d2db5dff91d323](https://i.gyazo.com/88623573a0531e5133d2db5dff91d323.png)](https://gyazo.com/88623573a0531e5133d2db5dff91d323)  

Пример работы приложения:  
  
[![https://gyazo.com/cb8b494680cdf0b412ff162bb187099e](https://i.gyazo.com/cb8b494680cdf0b412ff162bb187099e.gif)](https://gyazo.com/cb8b494680cdf0b412ff162bb187099e)

**Инструменты:**  
JDK 7, Spring 4, JDBC, JUnit 4.11, Mockito 1.10.19, slf4j / log4j2, Jackson, JSP, jQuery 1.11.3, Twitter Bootstrap 3.3.6, Maven 3, Git / Bitbucket, Tomcat 8, MySQL, H2, IntelliJIDEA 14.  

**Заметки:**  
SQL скрипт создания модели БД расположен в `dao/src/main/resources/sqlscripts/smssender_db_init.sql`  
  
**Корнеев Михаил**  
Тренинг getJavaJob,   
[http://www.getjavajob.com](http://www.getjavajob.com)