package com.getjavajob.training.korneevm.smssender.logic;

import com.getjavajob.training.korneevm.smssender.dao.SmsHistoryDao;
import com.getjavajob.training.korneevm.smssender.logic.impl.SmsHistoryServiceImpl;
import com.getjavajob.training.korneevm.smssender.model.SmsHistory;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.verify;

/**
 * Created by mkorn on 25.02.2016.
 */
@RunWith(MockitoJUnitRunner.class)
public class SmsHistoryServiceImplTest {

    @Mock
    private SmsHistoryDao mockedSmsHistoryDao;

    @InjectMocks
    private SmsHistoryService smsHistoryService = new SmsHistoryServiceImpl();

    @Before
    public void initMocks() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void addWasCalled() {
        SmsHistory smsHistory = new SmsHistory();
        smsHistoryService.add(smsHistory);
        verify(mockedSmsHistoryDao).add(smsHistory);
    }

    @Test
    public void getWasCalled() {
        smsHistoryService.get(1);
        verify(mockedSmsHistoryDao).get(1);
    }

    @Test
    public void getAllWasCalled() {
        smsHistoryService.getAll();
        verify(mockedSmsHistoryDao).getAll();
    }
}
