package com.getjavajob.training.korneevm.smssender.logic;

import com.getjavajob.training.korneevm.smssender.model.BaseEntity;

import java.util.List;

/**
 * Created by mkorn on 25.02.2016.
 */
public interface CrudService<T extends BaseEntity> {

    void add(T entity);

    T get(int id);

    List<T> getAll();

}
