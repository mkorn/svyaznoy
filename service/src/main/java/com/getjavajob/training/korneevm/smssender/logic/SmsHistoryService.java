package com.getjavajob.training.korneevm.smssender.logic;

import com.getjavajob.training.korneevm.smssender.model.SmsHistory;

/**
 * Created by mkorn on 25.02.2016.
 */
public interface SmsHistoryService extends CrudService<SmsHistory> {
}
