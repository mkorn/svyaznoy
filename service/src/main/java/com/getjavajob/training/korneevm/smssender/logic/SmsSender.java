package com.getjavajob.training.korneevm.smssender.logic;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.Random;

/**
 * Created by mkorn on 26.02.2016.
 */
@Service
public class SmsSender {

    final Logger log = LoggerFactory.getLogger(getClass());
    final Random random = new Random();

    public boolean sendSms(String phone, String message) {
        boolean success = random.nextBoolean();
        log.info("message sent to {} with status {}:\n{}", new Object[]{phone, success, message});
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return success;
    }
}
