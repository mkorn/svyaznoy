package com.getjavajob.training.korneevm.smssender.logic.impl;

import com.getjavajob.training.korneevm.smssender.dao.SmsHistoryDao;
import com.getjavajob.training.korneevm.smssender.logic.SmsHistoryService;
import com.getjavajob.training.korneevm.smssender.model.SmsHistory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by mkorn on 25.02.2016.
 */
@Service
public class SmsHistoryServiceImpl implements SmsHistoryService {

    private static final Logger logger = LoggerFactory.getLogger(SmsHistoryServiceImpl.class);

    @Autowired
    private SmsHistoryDao smsHistoryDao;

    @Transactional
    @Override
    public void add(SmsHistory smsHistory) {
        logger.info("Service: adding new SmsHistory...");
        smsHistoryDao.add(smsHistory);
        logger.info("Service: new smsHistory was added");
    }

    @Override
    public SmsHistory get(int id) {
        logger.info("Service: getting SmsHistory by id={}", id);
        return smsHistoryDao.get(id);
    }

    @Override
    public List<SmsHistory> getAll() {
        logger.info("Service: getting all SmsHistories");
        return smsHistoryDao.getAll();
    }
}
