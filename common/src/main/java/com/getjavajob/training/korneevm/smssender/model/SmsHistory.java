package com.getjavajob.training.korneevm.smssender.model;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.getjavajob.training.korneevm.smssender.model.util.CustomDateSerializer;

import java.util.Date;
import java.util.Objects;

/**
 * Created by mkorn on 24.02.2016.
 */
public class SmsHistory extends BaseEntity {

    private String phone;

    @JsonSerialize(using = CustomDateSerializer.class)
    private Date sendDate;

    private String sentStatus;

    private String message;

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Date getSendDate() {
        return sendDate;
    }

    public void setSendDate(Date sendDate) {
        this.sendDate = sendDate;
    }

    public String getSentStatus() {
        return sentStatus;
    }

    public void setSentStatus(String sentStatus) {
        this.sentStatus = sentStatus;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        SmsHistory other = (SmsHistory) obj;
        return Objects.equals(getPhone(), other.getPhone())
                && Objects.equals(getSendDate(), other.getSendDate())
                && Objects.equals(getSentStatus(), other.getSentStatus())
                && Objects.equals(getMessage(), other.getMessage());
    }

    @Override
    public int hashCode() {
        int hash = 17;
        hash = hash * 31 + (getPhone() != null ? getPhone().hashCode() : 0);
        hash = hash * 31 + (getSendDate() != null ? getSendDate().hashCode() : 0);
        hash = hash * 31 + (getSentStatus() != null ? getSentStatus().hashCode() : 0);
        hash = hash * 31 + (getMessage() != null ? getMessage().hashCode() : 0);
        return hash;
    }

    @Override
    public String toString() {
        return "SmsHistory{" +
                "phone='" + phone + '\'' +
                ", sendDate=" + sendDate +
                ", sentStatus='" + sentStatus + '\'' +
                ", message='" + message + '\'' +
                '}';
    }
}
