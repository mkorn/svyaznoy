package com.getjavajob.training.korneevm.smssender.model;

/**
 * Created by mkorn on 24.02.2016.
 */
public abstract class BaseEntity {

    private int id;

    public BaseEntity() {
    }

    public BaseEntity(int id) {
        this.id = id;
    }

    public long getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
