<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
    <title>История отправки SMS</title>
    <!-- Bootstrap -->
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/bootstrap/css/bootstrap.min.css"/>">
    <!-- DataTables -->
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/datatables/datatables.min.css"/>">
    <!-- Custom CSS -->
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/style.css"/>">
</head>
<body>
<div id="content" class="container">
    <div id="sms-send-box">
        <h3>Отправка SMS</h3>

        <form id="sms-form" method="post" data-toggle="validator">
            <!-- Номер телефона -->
            <div class="form-group">
                <label for="phone">Номер телефона:</label>

                <div class="help-block with-errors"></div>
                <input type="text" pattern="^79\d{9}$" class="form-control" id="phone"
                       placeholder="Введите номер телефона" name="phone"
                       value="" required>

                <p class="help-block">Пример ввода: 79295100000</p>
            </div>
            <!-- Сообщение -->
            <div class="form-group">
                <label for="message">Сообщение:</label>

                <div class="help-block with-errors"></div>
                <textarea class="form-control" rows="3" maxlength="160" id="message" placeholder="Введите SMS сообщение"
                          name="message" required></textarea>

                <p class="help-block">Максимальная длина сообщения 160 символов</p>
            </div>
            <button type="submit" id="send-button" class="btn btn-primary" form="sms-form">Отправить</button>
        </form>
    </div>

    <div id="sms-status"></div>

    <div id="sms-history">
        <h3>История отправки SMS</h3>
        <table id="sms-status-table" class="table table-bordered table-striped">
            <thead>
            <tr>
                <td>Номер телефона</td>
                <td>Дата отправки</td>
                <td>Статус отправки</td>
                <td>SMS сообщение</td>
            </tr>
            </thead>
            <tbody>
            <c:forEach var="smsHistory" items="${smsHistories}" varStatus="loop">
                <tr>
                    <td>${smsHistory.phone}</td>
                    <td><fmt:formatDate value="${smsHistory.sendDate}" pattern="yyyy-MM-dd HH:mm:ss"/></td>
                    <td>${smsHistory.sentStatus}</td>
                    <td>${smsHistory.message}</td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </div>
</div>
<script type="text/javascript">var myContextPath = "${pageContext.request.contextPath}"</script>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script type="text/javascript" src="<c:url value = "/resources/bootstrap/js/jquery-1.11.3.min.js"/>"></script>
<!-- Bootstrap -->
<script type="text/javascript" src="<c:url value = "/resources/bootstrap/js/bootstrap.min.js"/>"></script>
<!-- DataTables -->
<script type="text/javascript" src="<c:url value = "/resources/datatables/datatables.min.js"/>"></script>
<script type="text/javascript" src="<c:url value = "/resources/js/dataTableConfig.js"/>"></script>
<!-- Bootstrap form validator -->
<script type="text/javascript" src="<c:url value = "/resources/validate/validator.js"/>"></script>
<!-- Custom JavaScript -->
<script type="text/javascript" src="<c:url value = "/resources/js/customjs.js"/>"></script>
</body>
</html>
