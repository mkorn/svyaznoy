$(document).ready(function () {

    $('#sms-form').validator().on('submit', function (e) {
        if (!e.isDefaultPrevented()) {
            e.preventDefault();
            var phone = $('#phone').val();
            var message = $('#message').val();
            setReadonly(true);
            $('#sms-status').html('<h4 style="color: green;">Отправка сообщения...</h4>');
            $.ajax({
                type: 'POST',
                url: myContextPath + '/sms-service',
                data: "phone=" + phone + "&message=" + message,
                success: function (data) {
                    $('#sms-status').html('<h4 style="color: green;">Сообщение успешно отправлено!</h4>');
                    var t = $('#sms-status-table').DataTable();
                    t.row.add([
                        data.phone,
                        data.sendDate,
                        data.sentStatus,
                        data.message
                    ]).draw(false);
                    $('#sms-form')[0].reset();
                    setReadonly(false);
                },
                error: function () {
                    $('#sms-status').html('<h4 style="color: red;">Произошла ошибка, сообщение не отправлено!</h4>');
                    setReadonly(false);
                }
            });
        }
    });

    function setReadonly(flag) {
        $("#phone").prop("readonly", flag);
        $("#message").prop("readonly", flag);
        $("#send-button").attr("disabled", flag);
    }

});