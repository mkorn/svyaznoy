package com.getjavajob.training.korneevm.smssender.ui;

import com.getjavajob.training.korneevm.smssender.logic.SmsHistoryService;
import com.getjavajob.training.korneevm.smssender.logic.SmsSender;
import com.getjavajob.training.korneevm.smssender.model.SmsHistory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import java.util.Date;
import java.util.List;

/**
 * Created by mkorn on 25.02.2016.
 */
@RestController
@RequestMapping(value = "/sms-service")
public class SmsController {

    private static final Logger logger = LoggerFactory.getLogger(SmsController.class);

    @Autowired
    private SmsHistoryService smsHistoryService;

    @Autowired
    private SmsSender smsSender;

    @RequestMapping(value = "/show")
    public ModelAndView showInfo() {
        logger.info("Showing SMS Service page with history");
        List<SmsHistory> smsHistoryList = smsHistoryService.getAll();
        ModelAndView modelAndView = new ModelAndView("smsInfo");
        modelAndView.addObject("smsHistories", smsHistoryList);
        logger.info("SMS history was added to the model");
        return modelAndView;
    }

    @RequestMapping(method = RequestMethod.GET)
    public List<SmsHistory> getSmsHistory() {
        logger.info("Getting all sending information from sms_history_tbl...");
        return smsHistoryService.getAll();
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<SmsHistory> putSmsHistory(@ModelAttribute SmsHistory smsHistory) throws Exception {
        logger.info("SMS sending...");
        logger.info("Phone={}", smsHistory.getPhone());
        logger.info("Message={}", smsHistory.getMessage());
        boolean success = smsSender.sendSms(smsHistory.getPhone(), smsHistory.getMessage());
        smsHistory.setSendDate(new Date());
        if (!success) {
            logger.warn("An error occurred while sending SMS");
            smsHistory.setSentStatus("Не отправлено");
            logger.info("Adding sending information to sms_history_tbl...");
            smsHistoryService.add(smsHistory);
            logger.info("Sending information was added to sms_history_tbl");
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        } else {
            logger.info("SMS was sent successfully");
            smsHistory.setSentStatus("Отправлено");
            logger.info("Adding sending information to sms_history_tbl...");
            smsHistoryService.add(smsHistory);
            logger.info("Sending information was added to sms_history_tbl");
            return ResponseEntity.ok(smsHistory);
        }
    }
}
