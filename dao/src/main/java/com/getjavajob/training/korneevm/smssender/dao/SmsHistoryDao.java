package com.getjavajob.training.korneevm.smssender.dao;

import com.getjavajob.training.korneevm.smssender.model.SmsHistory;

/**
 * Created by mkorn on 24.02.2016.
 */
public interface SmsHistoryDao extends CrudDao<SmsHistory> {
}
