package com.getjavajob.training.korneevm.smssender.dao;

import com.getjavajob.training.korneevm.smssender.model.BaseEntity;

import java.util.List;

/**
 * Created by mkorn on 24.02.2016.
 */
public interface CrudDao<T extends BaseEntity> {

    void add(T entity);

    T get(int id);

    List<T> getAll();

}
