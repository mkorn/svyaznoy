package com.getjavajob.training.korneevm.smssender.dao.jdbc;

import com.getjavajob.training.korneevm.smssender.dao.SmsHistoryDao;
import com.getjavajob.training.korneevm.smssender.model.SmsHistory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.Date;
import java.util.List;

/**
 * Created by mkorn on 24.02.2016.
 */
@Repository
public class SmsHistoryJdbcDao implements SmsHistoryDao {

    private static final Logger logger = LoggerFactory.getLogger(SmsHistoryJdbcDao.class);

    private static final String TABLE_NAME = "smssender.sms_history_tbl";
    private static final String INSERT =
            "INSERT INTO " + TABLE_NAME +
                    " (phone, " +
                    "sent_date, " +
                    "sent_status, " +
                    "message) " +
                    "VALUES " +
                    "(?,?,?,?);";

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public void add(final SmsHistory smsHistory) {
        logger.info("DAO: adding SmsHistory to sms_history_tbl...");
        KeyHolder generatedKeyHolder = new GeneratedKeyHolder();
        jdbcTemplate.update(new PreparedStatementCreator() {
            @Override
            public PreparedStatement createPreparedStatement(Connection con) throws SQLException {
                PreparedStatement ps = con.prepareStatement(getInsertStatement(), Statement.RETURN_GENERATED_KEYS);
                ps.setString(1, smsHistory.getPhone());
                ps.setTimestamp(2, new Timestamp(smsHistory.getSendDate().getTime()));
                ps.setString(3, smsHistory.getSentStatus());
                ps.setString(4, smsHistory.getMessage());
                return ps;
            }
        }, generatedKeyHolder);
        int lastId = generatedKeyHolder.getKey().intValue();
        smsHistory.setId(lastId);
        logger.info("DAO: SmsHistory was added to sms_history_tbl, id={}", lastId);
    }

    @Override
    public SmsHistory get(int id) {
        logger.info("DAO: getting SmsHistory by id={}, id");
        try {
            return jdbcTemplate.queryForObject(getSelectByIdStatement(), getRowMapper(), id);
        } catch (EmptyResultDataAccessException e) {
            logger.info("DAO: EmptyResultDataAccessException was catched, null will be returned");
            return null;
        }
    }

    @Override
    public List<SmsHistory> getAll() {
        logger.info("DAO: getting all SmsHistories from sms_history_tbl");
        try {
            return jdbcTemplate.query(getSelectAllStatement(), getRowMapper());
        } catch (EmptyResultDataAccessException e) {
            logger.info("DAO: EmptyResultDataAccessException was catched, null will be returned");
            return null;
        }
    }

    private RowMapper<SmsHistory> getRowMapper() {
        return new RowMapper<SmsHistory>() {
            @Override
            public SmsHistory mapRow(ResultSet rs, int rowNum) throws SQLException {
                SmsHistory smsHistory = new SmsHistory();
                smsHistory.setId(rs.getInt("id"));
                smsHistory.setPhone(rs.getString("phone"));
                Timestamp timestamp = rs.getTimestamp("sent_date");
                if (timestamp != null) {
                    smsHistory.setSendDate(new Date(timestamp.getTime()));
                }
                smsHistory.setSentStatus(rs.getString("sent_status"));
                smsHistory.setMessage(rs.getString("message"));
                return smsHistory;
            }
        };
    }

    private String getSelectAllStatement() {
        return "SELECT * FROM " + TABLE_NAME;
    }

    private String getSelectByIdStatement() {
        return getSelectAllStatement() + " WHERE id = ?";
    }

    private String getInsertStatement() {
        return INSERT;
    }
}
