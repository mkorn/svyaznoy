-- -----------------------------------------------------
-- Schema smssender
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS smssender;

-- -----------------------------------------------------
-- Schema smssender
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS smssender;

-- -----------------------------------------------------
-- Table smssender.sms_history_tbl
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS smssender.sms_history_tbl (
  id INT NOT NULL AUTO_INCREMENT COMMENT 'PK',
  phone VARCHAR(50) NOT NULL COMMENT 'Телефонный номер',
  sent_date DATETIME NOT NULL COMMENT 'Дата отправки SMS',
  sent_status VARCHAR(50) NOT NULL COMMENT 'Статус отправки SMS',
  message VARCHAR(255) NOT NULL COMMENT 'SMS сообщение',
  CONSTRAINT pk_sms_history_tbl_id PRIMARY KEY (id)
);

-- -----------------------------------------------------
-- Table smssender.sms_history_tbl init
-- -----------------------------------------------------
INSERT INTO smssender.sms_history_tbl
(phone,
sent_date,
sent_status,
message)
VALUES
('79110123457', timestamp '2015-06-07 08:00:11', 'Отправлено', 'приевет!'),
('79110123468', timestamp '2015-06-07 08:00:12', 'Не отправлено', 'пока!');

COMMIT;
