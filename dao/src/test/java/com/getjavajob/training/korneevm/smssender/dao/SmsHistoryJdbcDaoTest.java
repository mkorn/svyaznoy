package com.getjavajob.training.korneevm.smssender.dao;

import com.getjavajob.training.korneevm.smssender.model.SmsHistory;
import org.apache.commons.dbcp2.BasicDataSource;
import org.h2.tools.RunScript;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * Created by mkorn on 25.02.2016.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:dao-context.xml", "classpath:dao-context-overrides.xml"})
public class SmsHistoryJdbcDaoTest {

    private static final String INIT_SCRIPT = "h2init.sql";

    @Autowired
    private BasicDataSource dataSource;

    @Autowired
    private SmsHistoryDao smsHistoryDao;

    @Before
    public void dbInit() throws SQLException {
        Connection connection = dataSource.getConnection();
        RunScript.execute(connection, new InputStreamReader(this.getClass().getClassLoader().getResourceAsStream(INIT_SCRIPT)));
        connection.close();
    }

    @Test
    public void add() throws ParseException {
        SmsHistory testSmsHistory = new SmsHistory();
        testSmsHistory.setPhone("79295000102");
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        Date sentDate = format.parse("2016-02-25 10:05:40");
        testSmsHistory.setSendDate(sentDate);
        testSmsHistory.setSentStatus("success");
        testSmsHistory.setMessage("test message");
        smsHistoryDao.add(testSmsHistory);
        SmsHistory actualSmsHistory = smsHistoryDao.get(3);
        assertEquals(testSmsHistory, actualSmsHistory);
    }

    @Test
    public void getAll() {
        SmsHistory testSmsHistory1 = smsHistoryDao.get(1);
        SmsHistory testSmsHistory2 = smsHistoryDao.get(2);
        List<SmsHistory> testSmsHistoryList = new ArrayList<>(Arrays.asList(testSmsHistory1, testSmsHistory2));
        List<SmsHistory> actualSmsHistoryList = smsHistoryDao.getAll();
        assertEquals(testSmsHistoryList, actualSmsHistoryList);
    }
}
